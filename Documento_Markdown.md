# Exercicis Git
Creació d'un document on es defineix com serà un videojoc

## 1. Iniciar repositori
- Guardar tots els documents en una carpeta anomenada gitBasic  
`mkdir gitBasic`  

- Crear repositori git dins de la carpeta  
`cd gitBasic`  
`git init`  
Al directori es pot observar que s'ha creat un arxiu de git  
![Document git](./images/archivo_git.PNG "Document git")

- Veure l'estat del repositori  
`git status`

- Crear un document de text amb el nom "videojoc.txt" per escriure la definició del joc. Crear el document amb `nano videojoc.txt`  
![Document videojoc](./images/creacion_documento_videojocs.PNG "Document videojoc")

- Mirar l'estat del repositori  
![Estat repositori amb el document nou](./images/gitstatus_nuevo_doc_videojocs.PNG "Estat del repositori")

- Afegir els canvis a stage  
`git add .` 

- Mirar l'estat del repositori desprès de fer stage  
![Estat del repositori](./images/gitstatus_despues_de_gitadd.PNG "Estat del repositori")

- Afegir els canvis al repositori amb el missatge "inici document videojoc"  
![Commit del primer document](./images/gitcommit_de_firstcommit.PNG "Commit del primer document")

- Mirar l'estat del repositori  
![Estat del repositori](./images/gitstatus_despues_de_gitcommit.PNG "Estat del repositori")

- Veure la llista de commits que s'han fet al repositori  
`git log`


## 2. Modificació del document
- Afegir canvis al document i registrar els canvis  
Editar document, `git add .`, `git commit` i `git push` a master

## 3. Ignorar fitxers
- Crear fitxer que no es vol guardar a git  
`nano privat.txt`

- Crear fitxer .gitignore  
`nano .gitignore` i escriure `privat.txt`  
A l'estat del repositori ja no apareix el fitxer privat.txt


## 4. Treballar amb branques
- Crear branca nova  
`git branch game_system`

- Mirar l'estat per comprovar que treballem en la branca nova  
![Estat de branca game_system](./images/gitstatus_en_nueva_rama.PNG "Estat de branca game_system")

#### Modificar arxius
- Afegir text  al final del document i registrar els canvis  
Editar document, `git add .`, `git commit` i `git push`

- Mirar l'estat de la branca avans de canviar  
![Estat de la branca game_system](./images/gitstatus_en_nueva_rama.PNG "Estat de branca game_system")

- Canviar a la branca master  
`git checkout master`  
![Canviar a branca master](./images/gitcheckout_a_master.PNG "Canviar a branca master")

- La branca master no té els canvis del sistema de joc  
![Document videojoc](./images/corregit_document_videojocs.PNG "Document videojocs")

- Corregir el document i registrar els canvis a git  
![Registre dels canvis al document](./images/gitcommit_y_gitpush_de_master.PNG "Registre dels canvis al document") 

#### Merge
- Assegurar que som a la branca master  
`git status`

- Aplicar els canvis  
`git merge game_system`  
![Merge de game_system](./images/gitmerge_gamesystem.PNG "Merge de game_system")

#### Eliminar la branca 
- Llistar les branques del repositori  
`git branch`

- Eliminar branca  
`git branch -d game_system`  
![Llistat de branques i esborrat de game_system](./images/eliminar_rama_gamesystem.PNG "Llistat de branques i esborrat de game_system")









